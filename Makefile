.POSIX:
.SUFFIXES:

HARE = hare
PREFIX = /usr/local
SRCDIR = $(PREFIX)/src
BINDIR = $(PREFIX)/bin
HARESRCDIR = $(SRCDIR)/hare
THIRDPARTYDIR = $(HARESRCDIR)/third-party

dnsstamp:
	$(HARE) build $(HAREFLAGS) -o $@ cmd/dnsstamp

clean:
	rm -f dnsstamp

install-binary: dnsstamp
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f dnsstamp $(DESTDIR)$(BINDIR)/

install-module:
	mkdir -p $(DESTDIR)$(THIRDPARTYDIR)/encoding/dnsstamp
	cp -f encoding/dnsstamp/* $(DESTDIR)$(THIRDPARTYDIR)/encoding/dnsstamp/

install: install-binary install-module

uninstall:
	rm -rf $(DESTDIR)$(THIRDPARTYDIR)/encoding/dnsstamp
	rm -f  $(DESTDIR)$(BINDIR)/dnsstamp
